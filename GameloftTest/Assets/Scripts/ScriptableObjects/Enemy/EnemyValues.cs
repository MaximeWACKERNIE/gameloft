﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyValue", menuName = "AI/EnemyValues", order = 1)]
public class EnemyValues : ScriptableObject
{
    public GameObject prefab;

    public Vector3 scale;
    public int health;
    public float speed;

    public bool showHealth;

    public EnemySplitValues splitValues;

    [HideInInspector] public int level;
}
