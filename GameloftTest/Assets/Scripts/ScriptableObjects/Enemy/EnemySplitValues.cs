﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemySplitValue", menuName = "AI/EnemySplitValues", order = 2)]
public class EnemySplitValues : ScriptableObject
{
    public int spawnCount;
    public int spawnLevel;

    public EnemySplitValues spawnedEnemySplitValue;
}
