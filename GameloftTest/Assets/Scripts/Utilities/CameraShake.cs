﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    private static CameraShake _instance;

    [SerializeField] public Transform _camTransform;
    [SerializeField] private float _decreaseFactor = 1.0f;

    private Vector3 _startLocalPosition;

    void Awake()
    {
        _instance = this;
    }

    public IEnumerator ShakeCoroutine(float shakeAmount = 0.7f, float shakeDuration = 0.1f)
    {
        float time = 0.0f;
        while (time < shakeDuration)
        {
            time += Time.deltaTime;

            if (shakeDuration > 0)
            {
                Vector3 res = Vector3.zero + Random.insideUnitSphere * shakeAmount;
                res.z = _startLocalPosition.z;

                _camTransform.localPosition = res;

                shakeDuration -= Time.deltaTime * _decreaseFactor;
            }
            if (shakeAmount > 0)
            {
                shakeAmount -= Time.deltaTime * _decreaseFactor;
            }

            yield return null;
        }

        _camTransform.localPosition = _startLocalPosition;
    }

    public static void StartShake(float shakeAmount = 0.1f, float shakeDuration = 0.5f)
    {
        _instance._startLocalPosition = _instance.transform.localPosition;

        _instance.StartCoroutine(_instance.ShakeCoroutine(shakeAmount, shakeDuration));
    }
}