﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingImage : MonoBehaviour
{
    [SerializeField] private Color _blinkColor;

    [SerializeField] private bool _changeColorOnBlink;

    [SerializeField] private float _enabledDuration;
    [SerializeField] private float _disabledDuration;

    private Image _image;
    private Color _defaultColor;

    private bool _isEnabled;

    private Coroutine _blinkCoroutine;

    private void Awake()
    {
        _isEnabled = true;

        _image = GetComponent<Image>();
        _defaultColor = _image.color;
    }

    public void StartBlink()
    {
        if (_changeColorOnBlink)
        {
            _image.color = _blinkColor;
        }

        _blinkCoroutine = StartCoroutine(BlinkCoroutine());
    }

    public void StopBlink()
    {
        if (_changeColorOnBlink)
        {
            _image.color = _defaultColor;
        }

        _image.enabled = true;

        StopCoroutine(_blinkCoroutine);
        _blinkCoroutine = null;
    }

    IEnumerator BlinkCoroutine()
    {
        while (true)
        {
            _isEnabled = !_isEnabled;

            if (_isEnabled)
            {
                _image.enabled = true;
                yield return new WaitForSeconds(_enabledDuration);
            }
            else
            {
                _image.enabled = false;
                yield return new WaitForSeconds(_disabledDuration);
            }
        }
    }
}
