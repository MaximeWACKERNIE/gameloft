﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GenericPool<PoolObjectType> where PoolObjectType : PoolObject
{
    public enum OBJECT_CREATION_OPTION : uint
    {
        RESIZE_POOL = 0,
        DOES_NOT_CREATE_OBJECT
    }
    
    private PoolObjectType          _poolObjectPrefab = null;
    private List<PoolObjectType>    _objectList = null;
    private int                     _activeObjectCount = 0;

    private OBJECT_CREATION_OPTION  _objectCreationBehaviorWhenFull = OBJECT_CREATION_OPTION.DOES_NOT_CREATE_OBJECT;

    public OBJECT_CREATION_OPTION ObjectCreationBehaviorWhenFull { set { _objectCreationBehaviorWhenFull = value; } }

    public int ActiveObjectCount { get { return _activeObjectCount; } }

    public GenericPool(int poolSize, PoolObjectType poolObjectPrefab,
        OBJECT_CREATION_OPTION objectCreationBehaviorWhenFull = OBJECT_CREATION_OPTION.DOES_NOT_CREATE_OBJECT, Transform poolObjectParent = null)
    {
        _objectList = new List<PoolObjectType>(poolSize);
        _poolObjectPrefab = poolObjectPrefab;
        _objectCreationBehaviorWhenFull = objectCreationBehaviorWhenFull;

        for (int i = 0; i < poolSize; ++i)
        {
            PoolObjectType pool_object = Object.Instantiate(_poolObjectPrefab, poolObjectParent);
            pool_object.name = "PoolObject_" + i;
            pool_object.OnDeactivateEvent += DeactivatePoolObject;
            pool_object.gameObject.SetActive(false);

            _objectList.Add(pool_object);
        }
    }

    public PoolObjectType SpawnObjectFromPool(Vector3 position)
    {
        PoolObjectType poolObject = null;

        if (_activeObjectCount < _objectList.Count)
        {
            poolObject = ActivatePoolObject();
            poolObject.transform.position = position;

            return poolObject;
        }
        else // Pool is full.
        {
            switch (_objectCreationBehaviorWhenFull)
            {
                case OBJECT_CREATION_OPTION.DOES_NOT_CREATE_OBJECT: // Do nothing.
                    return null;

                case OBJECT_CREATION_OPTION.RESIZE_POOL:
                    {
                        poolObject = Object.Instantiate(_poolObjectPrefab);
                        poolObject.OnDeactivateEvent += DeactivatePoolObject;

                        _objectList.Add(poolObject);

                        return poolObject;
                    }
            }
        }

        return null;
    }

    private PoolObjectType ActivatePoolObject()
    {
        PoolObjectType poolObject = _objectList[_activeObjectCount];
        poolObject._id = _activeObjectCount;
        ++_activeObjectCount;

        poolObject.EnablePoolObject();

        return poolObject;
    }

    private void DeactivatePoolObject(int index)
    {
        if (index < _activeObjectCount)
        {
            _objectList[index].DisablePoolObject();

            --_activeObjectCount;

            if (index == _activeObjectCount)
                return;

            PoolObjectType temp = _objectList[_activeObjectCount];


            _objectList[_activeObjectCount] = _objectList[index];
            _objectList[_activeObjectCount]._id = _activeObjectCount;

            _objectList[index] = temp;
            _objectList[index]._id = index;
        }
        else
        {
            Debug.Log("Error in DeactivatePoolObject");
            Debug.Log("index: " + index);
            Debug.Log("_activeObjectCount: " + _activeObjectCount);
            Debug.Break();
        }
    }

    public void Update()
    {
        for (int i = 0; i < _activeObjectCount; ++i)
            _objectList[i].UpdateObject();
    }

    public List<PoolObjectType> GetAllActiveObjects()
    {
        if (_activeObjectCount <= 0)
            return null;

        List<PoolObjectType> res = _objectList.GetRange(0, _activeObjectCount);
        return res;
    }
}
