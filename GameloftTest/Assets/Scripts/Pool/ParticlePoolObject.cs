﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePoolObject : PoolObject
{
    private ParticleSystem _mainpPrticleSystem;
    private ParticleSystem[] _childrenParticleSystem;

    private float _psLifetime;

    private void Awake()
    {
        _mainpPrticleSystem = GetComponent<ParticleSystem>();
        _childrenParticleSystem = GetComponentsInChildren<ParticleSystem>();
        _psLifetime = _mainpPrticleSystem.main.duration;
    }

    public override void ClearObjectReferences()
    {
    }

    public override void EnablePoolObject()
    {
        base.EnablePoolObject();

        if (_mainpPrticleSystem != null)
            _mainpPrticleSystem.Play(true);

        StartCoroutine(ParticleSystemDestroyDelayCocroutine(_psLifetime));
       /* if (_childrenParticleSystem != null)
        {
            for (int i = 0; i < _childrenParticleSystem.Length; ++i)
                _childrenParticleSystem[i].Play(,);
        }*/
    }

    public override void DisablePoolObject()
    {
        base.DisablePoolObject();
    }

    public override void UpdateObject()
    {
    }

    IEnumerator ParticleSystemDestroyDelayCocroutine(float duration)
    {
        yield return new WaitForSeconds(duration);

        RaiseDeactivateEvent();
    }

    public void SetStartColor(Color color)
    {
        ParticleSystem.MainModule mainModule = _mainpPrticleSystem.main;
        mainModule.startColor = color;

        if (_childrenParticleSystem != null)
        {
            foreach (ParticleSystem ps in _childrenParticleSystem)
            {
                mainModule = ps.main;
                mainModule.startColor = color;
            }
        }
    }
}
