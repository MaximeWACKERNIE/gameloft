﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class PoolObject : MonoBehaviour
{
    [HideInInspector]
    public int _id = 0;

    public delegate void DeactivateEventHandler(int id);
    public event DeactivateEventHandler OnDeactivateEvent;

    public abstract void    UpdateObject();
    public abstract void    ClearObjectReferences();

    protected void RaiseDeactivateEvent()
    {
        ClearObjectReferences();

        if (OnDeactivateEvent != null)
            OnDeactivateEvent.Invoke(_id);
    }

    public virtual void EnablePoolObject()
    {
        gameObject.SetActive(true);
    }

    public virtual void DisablePoolObject()
    {
        gameObject.SetActive(false);
    }
}
