﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupMenu : MonoBehaviour
{
    [Header("Button Init")]
    [SerializeField] private GameObject _powerupButtonPrefab;
    [SerializeField] private int _powerupCount;
    [SerializeField] private bool _spawnPowerupButtons;

    [Header("Gameplay")]
    [SerializeField] private int _shipId;
    [SerializeField] [Range(0.0f, 1.0f)] private float _timeScaleOpen;

    public delegate void PowerupMenuDel();
    PowerupMenuDel OnMenuDisabled;

    //public BasePowerup CurrentPowerup {  get { return _currentPowerup; } }
    private BasePowerup _currentPowerup;
    private RectTransform _rectTransform;

    private Coroutine _cooldownCoroutine;
    private bool _isInCooldown;

    private GameObject _childGameObject;

    private void Awake()
    {
        _childGameObject = transform.GetChild(0).gameObject;
        _rectTransform = GetComponent<RectTransform>();

        if (_spawnPowerupButtons)
            SpawnPowerupButtons();
    }

    public void SubscribeToOnMenuDisabled(PowerupMenuDel del)
    {
        OnMenuDisabled += del;
    }

    public void UnsubscribeFromOnMenuDisabled(PowerupMenuDel del)
    {
        OnMenuDisabled -= del;
    }

    public void SetCurrentPowerup(BasePowerup power)
    {
        _currentPowerup = power;

        if (OnMenuDisabled != null)
            OnMenuDisabled.Invoke();

        CloseMenu();

        if (_currentPowerup.IsActive == false)
        {
            if (_isInCooldown)
            {
                StopCoroutine(_cooldownCoroutine);
                _cooldownCoroutine = null;
            }

            _cooldownCoroutine = StartCoroutine(CooldownCoroutine(power.Cooldown));
        }

        //Character.Instance.Ships[_shipId].SetPowerImageColor(power.PowerupImage.color);
        Character.Instance.Ships[_shipId].SetPowerImageSprite(power.PowerupImage.sprite);
    }

    public void AcitavPowerup()
    {
        if (_currentPowerup == null)
            return;

        if (_isInCooldown || _currentPowerup.IsActive)
        return;

        _currentPowerup.Activate(Character.Instance.Ships[_shipId]);
        Character.Instance.Ships[_shipId].PowerImage.fillAmount = 0.0f;
    }

    public void OnPowerEnded()
    {
        if (_currentPowerup._isReady == false)
            _cooldownCoroutine = StartCoroutine(CooldownCoroutine(_currentPowerup.Cooldown));
    }

    public void OnShipClick()
    {
        if (_childGameObject.activeInHierarchy)
            CloseMenu();
        else
            OpenMenu();
    }

    public void OnOtherClick()
    {
        CloseMenu();

        if (OnMenuDisabled != null)
            OnMenuDisabled.Invoke();
    }

    private void OpenMenu()
    {
        _childGameObject.SetActive(true);
        Time.timeScale = _timeScaleOpen;
    }

    private void CloseMenu()
    {
        Time.timeScale = 1.0f;

        _childGameObject.SetActive(false);

        if (OnMenuDisabled != null)
            OnMenuDisabled.Invoke();
    }

    public void ResetPCurrentPowerup()
    {
        if (_currentPowerup != null)
        {
            if (_cooldownCoroutine != null)
            {
                StopCoroutine(_cooldownCoroutine);
                _cooldownCoroutine = null;
                _isInCooldown = false;

                Character.Instance.Ships[_shipId].PowerImage.fillAmount = 1.0f;
            }

            _currentPowerup.PowerupReset();
        }
    }

    IEnumerator CooldownCoroutine(float duration)
    {
        _isInCooldown = true;

        float time = 0.0f;
        while (time < duration)
        {
            time += Time.deltaTime;

            Character.Instance.Ships[_shipId].PowerImage.fillAmount = time / duration;

            yield return null;
        }

        _isInCooldown = false;
        _currentPowerup._isReady = true;
        _cooldownCoroutine = null;
    }

    void SpawnPowerupButtons()
    {
        float heightOffset = _rectTransform.rect.height / ((float)_powerupCount / 2f);
        float widthOffeset = _rectTransform.rect.width / 4.0f;

        RectTransform currentRectTr;
        for (int i = 0; i < _powerupCount; ++i)
        {
            int count = i % 6;
            currentRectTr = Instantiate(_powerupButtonPrefab, transform).GetComponent<RectTransform>();

            // y pos
            float ypos = -(_rectTransform.rect.height / 2f);
            ypos += heightOffset * (count + 1);
            ypos -= heightOffset / 2f;

            // x pos
            float xpos;
            if (i < 6)
            {
                xpos = -(_rectTransform.rect.width / 2f);
                xpos += widthOffeset;
            }
            else
            {
                xpos = -(_rectTransform.rect.width / 2f);
                xpos += widthOffeset;

                xpos = -xpos;
            }

            Vector3 resVec = new Vector3(xpos, ypos, 0);

            print(xpos);
            currentRectTr.localPosition = resVec;
        }
    }
}
