﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipUpgrade 
{
    public ShipUpgradeData damage;
    public ShipUpgradeData speed;
}

[System.Serializable]
public struct UpgradeMultiplierData
{
    public float multiplier;
    public int maxUpgrades;
    public int baseCost;
}

public struct ShipUpgradeData
{
    public int cost;
    public int count;
}
