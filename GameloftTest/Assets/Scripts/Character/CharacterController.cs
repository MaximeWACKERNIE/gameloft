﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private LayerMask _shootSelectableLayerMask;
    [SerializeField] private LayerMask _spriteSelectableLayerMask;

    [SerializeField] private float _stayTouchShipDuration;

    private Character _character;

    private CharacterShip _currentShipSelected;
    private Coroutine _touchShipCoroutine;

    private void Awake()
    {
        _character = GetComponent<Character>();
    }

    private void Update()
    {
#if UNITY_EDITOR
        MouseInputUpdate();
#else
        InputTouchUpdate();
#endif
    }

    void MouseInputUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject() == false)
                TryShoot(Input.mousePosition);

            TrySprite(Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (_touchShipCoroutine != null)
            {
                StopCoroutine(_touchShipCoroutine);
                _currentShipSelected.AcitavPowerup();

                _touchShipCoroutine = null;
                SetCurrentShipSelected(null);
            }
        }
    }

    void InputTouchUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId) == false)
                {
                    TryShoot(touch.position);
                }

                TrySprite(Input.mousePosition, touch.fingerId);
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (_touchShipCoroutine != null)
                {
                    StopCoroutine(_touchShipCoroutine);
                    _currentShipSelected.AcitavPowerup();

                    _touchShipCoroutine = null;
                    SetCurrentShipSelected(null);
                }
            }
        }
    }

    void TryShoot(Vector2 touchPosition)
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touchPosition), Vector2.zero, 100f, _shootSelectableLayerMask);
        if (hit.transform != null)
        {
            _character.Shoot(hit.point);
        }
    }

    void TrySprite(Vector2 touchPosition, int touchId = -1)
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touchPosition), Vector2.zero, 100f, _spriteSelectableLayerMask);
        if (hit.transform != null)
        {
            CharacterShip selected = hit.transform.parent.GetComponent<CharacterShip>();
            // menu is open
            if (_currentShipSelected != null) // menu is open
            {
                if (selected == _currentShipSelected)
                {
                    _currentShipSelected.OnClick();
                    SetCurrentShipSelected(null);
                }
                else
                {
                    selected.AcitavPowerup();
                }
            }
            else // no menu open
            {
                SetCurrentShipSelected(selected);
                _touchShipCoroutine = StartCoroutine(OnShipTouchCouroutine(_stayTouchShipDuration));
            }
        }
#if UNITY_EDITOR
        else if (_currentShipSelected && EventSystem.current.IsPointerOverGameObject() == false)
#else
        else if (_currentShipSelected && EventSystem.current.IsPointerOverGameObject(touchId) == false)
#endif
        {
            _currentShipSelected.OnOtherClick();
        }
    }

    IEnumerator OnShipTouchCouroutine(float duration)
    {
        float time = 0.0f;
        while (time < duration)
        {
            time += Time.deltaTime;
            yield return null;
        }

        _currentShipSelected.OnClick();
        _touchShipCoroutine = null;
    }

    void SetCurrentShipSelected(CharacterShip selected)
    {
        if (_currentShipSelected)
            _currentShipSelected.MyPowerupMenu.UnsubscribeFromOnMenuDisabled(ShipMenuDisabled);

        _currentShipSelected = selected;

        if (selected != null)
            selected.MyPowerupMenu.SubscribeToOnMenuDisabled(ShipMenuDisabled);
    }

    void ShipMenuDisabled()
    {
        _currentShipSelected = null;
    }
}
