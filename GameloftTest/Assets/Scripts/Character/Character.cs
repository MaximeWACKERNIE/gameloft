﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    private static Character _instance;
    public static Character Instance
    {
        get { return _instance; }
    }

    [Header("Stack")]
    [SerializeField] private GameObject _stackPrefab;
    [SerializeField] private RectTransform _stackParent;
    [SerializeField] private int _stackCount;
    [SerializeField] private Color _startColor;
    [SerializeField] private Color _endColor;

    [Header("Gameplay")]
    [SerializeField] private int _damage;
    [SerializeField] private int _allStackShootCountMin;
    [SerializeField] private int _allStackShootCountMax;
    [SerializeField] private Color _bulletColor;

    public int Damage { get { return _damage; } }

    [SerializeField] private List<CharacterShip> _ships;
    public List<CharacterShip> Ships { get { return _ships; } }

    private int _currentStackcount;

    private void Awake()
    {
        _instance = this;

        SpawnStacks(_stackCount);
    }

    void SpawnStacks(int count)
    {
        float div = _stackParent.rect.height / (float)count;
        for (int i = 0; i < count; ++i)
        {
            GameObject spawned = Instantiate(_stackPrefab, _stackParent);
            RectTransform rectTransform = spawned.GetComponent<RectTransform>();

            // position
            float res = -(_stackParent.rect.height / 2f);
            res += div * (i + 1);
            res -= div / 2f;
            Vector3 resVec = new Vector3(0, res, 0);
            rectTransform.localPosition = resVec;

            // Color
            float colorLerp = (float)(i + 1) / (float)count;
            Color rescolor = Color.Lerp(_startColor, _endColor, colorLerp);
            spawned.GetComponent<Image>().color = rescolor;

            spawned.SetActive(false);
        }
    }

    public void UpdateShip(int id, ShipUpgrade upgrade)
    {
        foreach (CharacterShip ship in _ships)
        {
            if (id == ship.Id)
            {
                ship.UpdateValues(upgrade);
                return;
            }
        }
    }

    public void Shoot(Vector2 position)
    {
        if (IsFullStack())
        {
            AllStackShoot(position);
            ResetStack();
        }
        else
        {
            Bullet bullet = (Bullet)PoolManager.Instance.SpawnObjectFromPool("Bullet", position);
            bullet.Init(_damage, 5.0f, null, false, _bulletColor);

            AddStack();
        }

        PoolManager.Instance.SpawnObjectFromPool("Touch", position);
    }

    void AllStackShoot(Vector2 position)
    {
        CameraShake.StartShake();

        Bullet currentBullet;
        Enemy target;
        List<Enemy> toExclude = new List<Enemy>();

        int count = Random.Range(_allStackShootCountMin, _allStackShootCountMax);
        for (int i = 0; i < count; ++i)
        {
            currentBullet = (Bullet)PoolManager.Instance.SpawnObjectFromPool("Bullet", position);
            target = GameManager.Instance.GetClosesEnemy(position, toExclude);

            currentBullet.Init(_damage, 5.0f, target, false, _bulletColor);

            if (i + 1 < count)
             toExclude.Add(target);
        }
    }

    void ResetStack()
    {
        for (int i = 0; i < _stackParent.childCount; ++i)
        {
            _stackParent.GetChild(i).gameObject.SetActive(false);
        }
        _currentStackcount = 0;
    }

    void AddStack()
    {
        _stackParent.GetChild(_currentStackcount).gameObject.SetActive(true);
        ++_currentStackcount;
    }

    bool IsFullStack()
    {
        return _currentStackcount == _stackCount;
    }
}
