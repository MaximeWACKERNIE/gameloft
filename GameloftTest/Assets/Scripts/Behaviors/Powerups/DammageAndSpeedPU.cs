﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DammageAndSpeedPU : BasePowerup
{
    [SerializeField] private int _dammageBuff;
    [SerializeField] private float _fireRateBuff;

    [Header("Feedback")]
    [SerializeField] private Vector3 _scaleBuff;
    [SerializeField] private Color _colorbuff;

    private Vector3 _defaultScale;
    private Color _defaultColor;

    protected override void OnActivate()
    {
        _defaultScale = _owner.SpriteRenderer.transform.localScale;
        _defaultColor = _owner.SpriteRenderer.color;

        _owner.SpriteRenderer.transform.localScale += _scaleBuff;
        _owner.SpriteRenderer.color = _colorbuff;
        _owner.SetBulletColor(_colorbuff);

        _owner._bonusDamage += _dammageBuff;
        _owner._bonusFireRate += _fireRateBuff;
    }

    protected override void OnDeactivate()
    {
        _owner.SpriteRenderer.transform.localScale = _defaultScale;
        _owner.SpriteRenderer.color = _defaultColor;
        _owner.ResetBulletColor();


        _owner._bonusDamage -= _dammageBuff;
        _owner._bonusFireRate -= _fireRateBuff;
    }

    protected override void PowerupUpdate()
    {
    }

    public override void PowerupReset()
    {
        base.PowerupReset();
    }
}
