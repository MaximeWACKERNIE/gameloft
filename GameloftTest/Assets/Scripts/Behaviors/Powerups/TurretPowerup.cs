﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretPowerup : BasePowerup
{
    protected override void OnActivate()
    {
        _owner.ActivateTurrets();
    }

    protected override void OnDeactivate()
    {
        _owner.DeactivateTurrets();
    }

    protected override void PowerupUpdate()
    {
    }
}
