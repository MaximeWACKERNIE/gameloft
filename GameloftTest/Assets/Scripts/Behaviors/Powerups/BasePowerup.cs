﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BasePowerup : MonoBehaviour
{
    [SerializeField] protected float _duration;
    [SerializeField] protected float _cooldown;

    [SerializeField] protected bool _readyOnReset;
    [SerializeField] protected bool _continueOnReset;

    public float Duration { get { return _duration; } }
    public float Cooldown { get { return _cooldown; } }
    public bool IsActive { get { return _isActive; } }
    public bool ReadyOnReset { get { return _readyOnReset; } }

    public Image PowerupImage { get { return GetComponent<Image>(); } }

    protected PowerupMenu _menu;
    protected CharacterShip _owner;
    protected bool _isActive;

    [HideInInspector] public bool _isReady;

    protected float _currentTime;

    protected abstract void OnActivate();
    protected abstract void OnDeactivate();
    protected abstract void PowerupUpdate();

    private void Awake()
    {
        _isReady = false;

        _menu = GetComponentInParent<PowerupMenu>();
    }

    public void OnClick()
    {
        _menu.SetCurrentPowerup(this);
    }

    public void Activate(CharacterShip ship)
    {
        _owner = ship;
        _isActive = true;
        _isReady = false;

        OnActivate();

        ship.StartCoroutine(UpdateCoroutine(_duration));
    }

    public virtual void PowerupReset()
    {
        if (_readyOnReset)
            _isReady = true;

        if (_continueOnReset)
            _currentTime = 0.0f;
    }

    IEnumerator UpdateCoroutine(float duration)
    {
        _currentTime = 0.0f;
        while (_currentTime < duration)
        {
            PowerupUpdate();

            _currentTime += Time.deltaTime;
            yield return null;
        }


        _isActive = false;
        OnDeactivate();
        _menu.OnPowerEnded();
    }
}
