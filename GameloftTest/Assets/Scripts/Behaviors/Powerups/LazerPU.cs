﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerPU : BasePowerup
{
    [SerializeField] private int _damageBuff;
    [SerializeField] private float _speed;
    [SerializeField] private Vector3 _scale;

    protected override void OnActivate()
    {
        _owner.Shoot(_owner.FullDamage + _damageBuff, _speed, _scale, true);
    }

    protected override void OnDeactivate()
    {
    }

    protected override void PowerupUpdate()
    {

    }
}
