﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPU : BasePowerup
{
    protected override void OnActivate()
    {
        PowerupMenu[] powerupMenus = FindObjectsOfType<PowerupMenu>();
        if (powerupMenus != null)
        {
            foreach (PowerupMenu powerupMenu in powerupMenus)
            {
                powerupMenu.ResetPCurrentPowerup();
            }
        }
    }

    protected override void OnDeactivate()
    {
    }

    protected override void PowerupUpdate()
    {
    }

    public override void PowerupReset()
    {
        // no base
    }
}
