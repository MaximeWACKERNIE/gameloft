﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricPU : BaseMapEffectPU
{
    protected override void PowerupUpdate()
    {
    }

    protected override void OnEnemyLoopActivate(Enemy enemy)
    {
        base.OnEnemyLoopActivate(enemy);

        if (enemy.IsDead == false)
            enemy.EnterElectricState();
    }

    protected override void OnEnemyLoopDeactivate(Enemy enemy)
    {
        base.OnEnemyLoopDeactivate(enemy);

        enemy.ExitElectricState();
    }

    public override void PowerupReset()
    {
        // nothing
    }
}
