﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMapEffectPU : BasePowerup
{
    [SerializeField] private Coroutine _coroutine;
    [SerializeField] private ParticleSystem _particleSystem;

    [SerializeField] protected int _damage;

    [SerializeField] private bool _restartOnActivate;

    protected delegate void MapEffectPowerupOneParamDel(Enemy enemy);
    //MapEffectPowerupOneParamDel OnEnemyLoop;

    protected override void OnActivate()
    {
        if (_particleSystem != null)
        {
            if (_restartOnActivate && _particleSystem.isPlaying)
            {
                _particleSystem.Clear();
            }

            _particleSystem.Play();
        }

        LoopEnemies(OnEnemyLoopActivate);
    }

    protected override void OnDeactivate()
    {
        if (_particleSystem)
            _particleSystem.Stop(true);

        LoopEnemies(OnEnemyLoopDeactivate);

    }

    protected override void PowerupUpdate()
    {
    }

    private void LoopEnemies(MapEffectPowerupOneParamDel callback)
    {
        List<PoolObject> enemies = PoolManager.Instance.GetPool("Enemy").GetAllActiveObjects();
        if (enemies != null)
        {
            foreach (Enemy enemy in enemies)
            {
                callback.Invoke(enemy);
            }
        }
    }

    protected virtual void OnEnemyLoopActivate(Enemy enemy)
    {
        if (_damage != 0)
        {
            enemy.TakeDamage(_damage);
        }
    }

    protected virtual void OnEnemyLoopDeactivate(Enemy enemy)
    {
    }
}
