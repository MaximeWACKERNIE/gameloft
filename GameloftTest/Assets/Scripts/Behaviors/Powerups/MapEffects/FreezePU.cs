﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezePU : BaseMapEffectPU
{
    protected override void PowerupUpdate()
    {
    }

    protected override void OnEnemyLoopActivate(Enemy enemy)
    {
        base.OnEnemyLoopActivate(enemy);

        enemy.EnterFreezeState();
    }

    protected override void OnEnemyLoopDeactivate(Enemy enemy)
    {
        base.OnEnemyLoopDeactivate(enemy);

        enemy.ExitFreezeState();
    }

    public override void PowerupReset()
    {
        // nothing
    }
}
