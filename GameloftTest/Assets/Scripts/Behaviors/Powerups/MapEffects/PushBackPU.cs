﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushBackPU : BaseMapEffectPU
{
    [SerializeField] private float _yForce;

    protected override void PowerupUpdate()
    {
    }

    protected override void OnEnemyLoopActivate(Enemy enemy)
    {
        base.OnEnemyLoopActivate(enemy);

        enemy.Getrigidbody2D.AddForce(new Vector2(0, _yForce), ForceMode2D.Impulse);
    }
}
