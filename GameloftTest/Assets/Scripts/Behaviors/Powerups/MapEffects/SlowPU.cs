﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowPU : BaseMapEffectPU
{
    protected override void PowerupUpdate()
    {
    }

    protected override void OnEnemyLoopActivate(Enemy enemy)
    {
        base.OnEnemyLoopActivate(enemy);

        enemy.EnterSlowState();
    }

    protected override void OnEnemyLoopDeactivate(Enemy enemy)
    {
        base.OnEnemyLoopDeactivate(enemy);

        enemy.ExitSlowState();
    }

    public override void PowerupReset()
    {
        // nothing
    }
}
