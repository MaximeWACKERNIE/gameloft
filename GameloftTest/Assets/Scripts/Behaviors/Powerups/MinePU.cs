﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinePU : BasePowerup
{
    [SerializeField] float _maxRadius;

    [SerializeField] int _dammage;
    [SerializeField] int _mineCount;

    [SerializeField] Vector2 _center;

    protected override void OnActivate()
    {
        for (int i = 0; i < _mineCount; ++i)
        {
            Vector2 position = GetRandomPositionInArea(_center, _maxRadius);
            //Mine mine = GameManager.Instance.MinePool.SpawnObjectFromPool(_owner.BulletSpawnTranform.position);
            Mine mine = (Mine)PoolManager.Instance.SpawnObjectFromPool("Mine", _owner.BulletSpawnTranform.position);
            mine.StartDelayExplosion(position, _dammage);
        }
    }

    protected override void OnDeactivate()
    {
    }

    protected override void PowerupUpdate()
    {
    }

    Vector2 GetRandomPositionInArea(Vector2 center, float test)
    {
        return center + Random.insideUnitCircle * _maxRadius;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(_center, _maxRadius);
    }
}
