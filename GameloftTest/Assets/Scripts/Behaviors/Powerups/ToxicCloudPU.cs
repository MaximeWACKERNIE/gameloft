﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicCloudPU : BasePowerup
{
    [SerializeField] private int _damage;
    [SerializeField] private float _damageInterval;

    [SerializeField] private ToxicCloudCollider _toxicCloudGameObject;

    protected override void OnActivate()
    {
        _toxicCloudGameObject.gameObject.SetActive(true);
        _toxicCloudGameObject.Init(_damage, _damageInterval);
    }

    protected override void OnDeactivate()
    {
        _toxicCloudGameObject.gameObject.SetActive(false);
    }

    protected override void PowerupUpdate()
    {
    }
}
