﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAimCollider : MonoBehaviour
{
    public delegate void ShipAimColliderDel(Collider2D other);
    public ShipAimColliderDel OnAimTriggerEnter;
    public ShipAimColliderDel OnAimTriggerExit;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (OnAimTriggerEnter != null)
            OnAimTriggerEnter.Invoke(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (OnAimTriggerExit != null)
            OnAimTriggerExit.Invoke(collision);
    }

    public void SubscribeToOnAimTriggerEnter(ShipAimColliderDel del)
    {
        if (del != null)
            OnAimTriggerEnter += del;
    }

    public void SubscribeToOnAimTriggerExit(ShipAimColliderDel del)
    {
        if (del != null)
            OnAimTriggerExit += del;
    }
}
