﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterShip : BaseShip
{
    [Header("Upgrade")]
    [SerializeField] private int _dammageUpgrade;
    [SerializeField] private float _fireRateUpgrade;

    [SerializeField] private GameObject _turretsGameObject;

    [SerializeField] private int _id;
    [SerializeField] private PowerupMenu _powerupMenu;

    int _level = 0;

    public PowerupMenu MyPowerupMenu { get { return _powerupMenu; } }
    private Image _powerImage;
    public Image PowerImage { get { return _powerImage; } }

    public int Id
    {
        get { return _id; }
    }

    public delegate void ShipTwoParamIntDelegate(int id, int level);
    public ShipTwoParamIntDelegate OnLevelUpdated;

    protected override void Awake()
    {
        base.Awake();

        _powerImage = GetComponentInChildren<Image>();
    }

    private float FireRate { get { return _baseFireRate + _bonusFireRate; } }


    public void SubscribeToLevelUpdated(ShipTwoParamIntDelegate del)
    {
        if (del != null)
            OnLevelUpdated += del;
    }

    public void UpdateValues(ShipUpgrade upgrade)
    {
        _level = upgrade.damage.count + upgrade.speed.count;
        if (OnLevelUpdated != null)
            OnLevelUpdated.Invoke(_id, _level);

        _damage = _baseDamage + upgrade.damage.count * _dammageUpgrade;
        _fireRate = _baseFireRate + upgrade.speed.count * _fireRateUpgrade;
    }

    public void ActivateTurrets()
    {
        _turretsGameObject.SetActive(true);
    }

    public void DeactivateTurrets()
    {
        _turretsGameObject.SetActive(false);
    }

    public void AcitavPowerup()
    {
        _powerupMenu.AcitavPowerup();
    }

    public void OnClick()
    {
        _powerupMenu.OnShipClick();
    }

    public void OnOtherClick()
    {
        _powerupMenu.OnOtherClick();
    }


    public void SetPowerImageSprite(Sprite sprite)
    {
        _powerImage.sprite = sprite;
        _powerImage.color = Color.white;
    }

    public void SetPowerImageColor(Color color)
    {
        _powerImage.color = color;
    }
}
