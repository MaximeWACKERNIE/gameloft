﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseShip : MonoBehaviour
{
    [SerializeField] protected Transform _bulletSpawnTranform;
    [SerializeField] protected SpriteRenderer _spriteRenderer;

    [Header("Shoot")]
    [SerializeField] protected float _shootSpeed = 5.0f;
    [SerializeField] protected float _baseFireRate = 1.0f;
    [SerializeField] protected int _baseDamage = 1;

    [Header("Control")]
    protected float _fireRate;

    [SerializeField] protected float _rotationSpeed;

    protected int _damage;

    [HideInInspector] public int _bonusDamage;
    [HideInInspector] public float _bonusFireRate;


    [HideInInspector] public Collider2D _collider;

    protected bool _canShoot;

    protected Enemy _target;

    private bool _useBulleColor;
    private Color _bulletColor;

    public SpriteRenderer SpriteRenderer { get { return _spriteRenderer; } }
    public Transform BulletSpawnTranform { get { return _bulletSpawnTranform; } }

    public int FullDamage { get { return _damage + _bonusDamage; } }
    private float FullFireRate { get { return _fireRate + _bonusFireRate; } }

    protected virtual void Awake()
    {
        _damage = _baseDamage;
        _fireRate = _baseFireRate;

        ShipAimCollider shipAimCollider = GameObject.FindGameObjectWithTag("ShipCollider").GetComponent<ShipAimCollider>();
        _collider = shipAimCollider.GetComponent<Collider2D>();
        shipAimCollider.SubscribeToOnAimTriggerEnter(On2DTrigerEnter);
        shipAimCollider.SubscribeToOnAimTriggerExit(On2DTrigerExit);

        _canShoot = true;
    }

    protected virtual void Update()
    {
        UpdateLookAt();
        UpdateShoot();
    }

    public void On2DTrigerEnter(Collider2D other)
    {
        if (_target == null)
        {
            Enemy enemy = other.transform.parent.GetComponent<Enemy>();
            SetTarget(enemy);
        }
    }

    public void On2DTrigerExit(Collider2D other)
    {
        if (_target != null)
        {
            Enemy enemy = other.transform.parent.GetComponent<Enemy>();
            if (enemy == _target)
                SetTarget(null, true);
        }
    }

    void UpdateLookAt()
    {
        if (_target != null)
        {
            Vector3 diff = _target.CurrentPosition - transform.position;
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            Quaternion rot = Quaternion.Euler(0f, 0f, rot_z - 90);
            _spriteRenderer.transform.rotation = Quaternion.Slerp(_spriteRenderer.transform.rotation, rot, _rotationSpeed * Time.deltaTime);
        }
        else
        {
            _spriteRenderer.transform.rotation = Quaternion.Slerp(_spriteRenderer.transform.rotation, Quaternion.identity, _rotationSpeed * Time.deltaTime);
        }
    }

    void UpdateShoot()
    {
        if (_target != null && _canShoot)
        {
            _canShoot = false;
            Shoot();
        }
    }

    public void Shoot(int damage, float speed, Vector3 scale, bool useCollider)
    {
        Bullet bullet = (Bullet)PoolManager.Instance.SpawnObjectFromPool("Bullet", _bulletSpawnTranform.position);
        if (_useBulleColor == false)
            bullet.Init(damage, speed, _spriteRenderer.transform.up, scale, useCollider);
        else
            bullet.Init(damage, speed, _spriteRenderer.transform.up, scale, useCollider, false, _bulletColor);
    }

    void Shoot()
    {
        Bullet bullet = (Bullet)PoolManager.Instance.SpawnObjectFromPool("Bullet", _bulletSpawnTranform.position);
        if (_useBulleColor == false)
            bullet.Init(FullDamage, _shootSpeed, _target);
        else
            bullet.Init(FullDamage, _shootSpeed, _target, false, _bulletColor);

        StartCoroutine(CoolDownCoroutine(1.0f / FullFireRate));
    }

    IEnumerator CoolDownCoroutine(float duration)
    {
        float time = 0.0f;
        while (time < duration)
        {
            time += Time.deltaTime;
            yield return null;
        }

        _canShoot = true;
    }

    protected void SetTarget(Enemy newTarget, bool searchForNewTarget = false)
    {
        if (_target != null) // old target
        {
            _target.UnsubscribeFromOnDeath(TargetDeath);
        }

        if (newTarget != null) // new target
        {
            newTarget.SubscribeToOnDeath(TargetDeath);
        }

        _target = newTarget;

        if (newTarget == null && searchForNewTarget)
            SetTarget(GetClosestTarget());

    }

    void TargetDeath(Enemy target)
    {
        SetTarget(GetClosestTarget());
    }

    protected Enemy GetClosestTarget()
    {
        int maxColliders = PoolManager.Instance.GetPool("Enemy").ActiveObjectCount;
        Collider2D[] result = new Collider2D[maxColliders];

        ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.layerMask = 1 << LayerMask.NameToLayer("Enemy");
        contactFilter.useLayerMask = true;

        int count = _collider.OverlapCollider(contactFilter, result);
        float minDsitance = float.MaxValue;
        Enemy res = null;
        for (int i = 0; i < count; ++i)
        {
            Enemy current = result[i].transform.parent.GetComponent<Enemy>();

            float distance = Vector2.Distance(transform.position, current.transform.position);
            if (distance < minDsitance)
            {
                if (current != _target)
                {
                    minDsitance = distance;
                    res = current;
                }
            }
        }

        return res;
    }

    public void SetBulletColor(Color color)
    {
        _useBulleColor = true;
        _bulletColor = color;
    }

    public void ResetBulletColor()
    {
        _useBulleColor = false;
    }
}
