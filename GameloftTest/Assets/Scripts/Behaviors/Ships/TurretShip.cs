﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShip : BaseShip
{
    [SerializeField] private Color _turretBulletColor;

    private void Awake()
    {
        base.Awake();

        SetBulletColor(_turretBulletColor);
    }

    private void OnEnable()
    {
        SetTarget(GetClosestTarget());
    }

    private void OnDisable()
    {
        SetTarget(null);
        _canShoot = true;
        _spriteRenderer.transform.rotation = Quaternion.identity;
    }

    protected override void Update()
    {
        base.Update();
    }
}
