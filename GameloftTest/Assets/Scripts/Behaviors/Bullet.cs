﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : PoolObject
{
    [SerializeField] private float _explosionRadius;
    [SerializeField] private float _distanceToReachTarget;

    private Enemy _target = null;
    private Vector2 _targetPosition = new Vector2(0f, -10000f);
    private float _speed;
    private int _damage;
    private Vector3 _direction;

    private Vector3 _defaultScale;

    private TrailRenderer _trailRenderer;
    private CircleCollider2D _circleCollider2D;

    private Color _baseColor;

    private void Awake()
    {
        _defaultScale = transform.localScale;

        _circleCollider2D = GetComponent<CircleCollider2D>();
        _trailRenderer = GetComponent<TrailRenderer>();

        _baseColor = _trailRenderer.startColor;
    }

    public void Init(int damage, float speed, Vector2 dir, Vector3 scale, bool useCollider, bool randomColor = true, Color color = default(Color))
    {
        _trailRenderer.Clear();

        if (randomColor)
            _trailRenderer.startColor = GameManager.Instance.GetRandomBulletColor();
        else
            _trailRenderer.startColor = color;

        _damage = damage;
        _speed = speed;
        _direction = dir;

        transform.localScale = scale;

        if (useCollider)
            _circleCollider2D.enabled = true;
    }

    public void Init(int damage, float speed, Enemy target, bool randomColor = true, Color color = default(Color))
    {
        _trailRenderer.Clear();

        if (randomColor)
            _trailRenderer.startColor = GameManager.Instance.GetRandomBulletColor();
        else
            _trailRenderer.startColor = color;

        _damage = damage;
        _speed = speed;

        if (target != null)
        {
            SetTarget(target);
        }
        else
        {
            Enemy enemy = GameManager.Instance.GetClosesEnemy(transform.position);
            if (enemy != null)
            {
                SetTarget(enemy);
            }
            else
            {
                _direction = Vector3.up;
            }
        }
    }

    public override void UpdateObject() 
    {
        if (_target != null)
            _targetPosition = _target.CurrentPosition;

        if (HasReachedTarget(_targetPosition))
            Hit();
 
        if (transform.position.y > GameManager.Instance.YDeadZoneBullet)
            RaiseDeactivateEvent();

        UpdatePosition();
    }

    void UpdatePosition()
    {
        Vector3 dir;

        if (_target)
        {
            dir = _target.CurrentPosition - transform.position;
            dir.Normalize();
        }
        else
            dir = _direction;

        transform.position += dir * _speed * Time.deltaTime;
    }

    bool HasReachedTarget(Vector2 position)
    {
        float distance = Vector2.Distance(transform.position, position);
        return distance < _distanceToReachTarget;
    }

    void Hit()
    {
        if (_target != null)
        {
            _target.UnsubscribeFromOnDeath(TargetDeath);
        }

        Explose();

        RaiseDeactivateEvent();
    }

    private void Explose()
    {
        Vector3 position = transform.position;
        position.z -= 0.1f;
        ParticlePoolObject spawned = (ParticlePoolObject)PoolManager.Instance.SpawnObjectFromPool("BulletImpact", position);
        spawned.SetStartColor(_trailRenderer.startColor);

        LayerMask layer = 1 << LayerMask.NameToLayer("Enemy");
        Collider2D[] res = Physics2D.OverlapCircleAll(transform.position, _explosionRadius, layer);
        foreach (Collider2D collider in res)
        {
            Enemy enemy = collider.transform.GetComponentInParent<Enemy>();
            enemy.TakeDamage(_damage);
        }
    }

    private void SetTarget(Enemy newTarget)
    {
        if (newTarget != null) // new target
        {
            newTarget.SubscribeToOnDeath(TargetDeath);
            _target = newTarget;
        }
    }

    void TargetDeath(Enemy target)
    {
        _direction = target.CurrentPosition - transform.position;
        _direction.Normalize();

        _target.UnsubscribeFromOnDeath(TargetDeath);
        _target = null;
    }

    public override void EnablePoolObject()
    {
        base.EnablePoolObject();
    }

    public override void ClearObjectReferences() 
    {
        _target = null;
        _damage = 0;
        _speed = 0.0f;
        _direction = Vector3.zero;
        _targetPosition = new Vector2(0f, -10000f);
        _circleCollider2D.enabled = false;

        transform.localScale = _defaultScale;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.transform.GetComponentInParent<Enemy>();
        if (enemy != null)
        {
            enemy.TakeDamage(_damage);
        }
        else
        {
            print("Couldn't get enemy: " + collision.transform.name);
            Debug.Break();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _explosionRadius);
    }
}
