﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : PoolObject
{
    [SerializeField] List<EnemyValues> _enemyTypes;

    private int _currentEnemyId;

    private EnemyStruct _currentEnemy;

    public delegate void EnemyOneParamDelegate(Enemy enemy);
    public EnemyOneParamDelegate OnDeath;

    public Vector3 CurrentPosition { get { return _currentEnemy.myRigidbody2D.transform.position; } }
    public Rigidbody2D Getrigidbody2D { get { return _currentEnemy.myRigidbody2D; } }

    public bool IsDead { get { return _currentEnemy.health <= 0; } }
    private void Awake()
    {
        _currentEnemy = new EnemyStruct();
        foreach (EnemyValues value in _enemyTypes)
        {
            Transform tr = Instantiate(value.prefab, transform).transform;
            tr.localScale = value.scale;
            tr.gameObject.SetActive(false);
            if (value.showHealth == false)
            {
                tr.GetComponentInChildren<Canvas>().gameObject.SetActive(false);
            }
        }
    }

    private void Start()
    {
        SubscribeToOnDeath(GameManager.Instance.OnEnemyDeath);
    }

    private void FixedUpdate()
    {
        if (_currentEnemy.myRigidbody2D)
        {
            if (_isFrozen || _isElectric)
                return;

            float yVelocity = _isSlowed ? -_slowSpeed : -_currentEnemy.speed;
            Vector2 velocity = new Vector2(0, yVelocity);
            _currentEnemy.myRigidbody2D.MovePosition(_currentEnemy.myRigidbody2D.position + velocity * Time.fixedDeltaTime);
        }
    }

    public override void UpdateObject()
    {
        GameManager.Instance.IsInDangerZone(_currentEnemy.myRigidbody2D.transform.position);
        GameManager.Instance.IsInDeadZone(_currentEnemy.myRigidbody2D.transform.position);
    }

    public override void EnablePoolObject()
    {
        base.EnablePoolObject();
    }

    public override void ClearObjectReferences()
    {
        ExitFreezeState();
        ExitElectricState();

        _currentEnemy.spriteRenderer.color = _currentEnemy.defaultColor;
        _currentEnemy.myRigidbody2D.transform.localPosition = Vector3.zero;
        _currentEnemy.myRigidbody2D.gameObject.SetActive(false);
    }

    public void SetToLevel(int level)
    {
        --level; // level parem starts at 1
        if (level < _enemyTypes.Count)
        {
            transform.GetChild(level).gameObject.SetActive(true);

            EnemyValues values = _enemyTypes[level];
            _currentEnemy.health = values.health;
            _currentEnemy.speed = values.speed;
            _currentEnemy.healthText = transform.GetChild(level).GetComponentInChildren<Text>();
            _currentEnemy.myRigidbody2D = transform.GetChild(level).GetComponent<Rigidbody2D>();
            _currentEnemy.spriteRenderer = transform.GetChild(level).GetComponent<SpriteRenderer>();
            _currentEnemy.showHealth = values.showHealth;
            _currentEnemy.level = level;
            _currentEnemy.split = values.splitValues;
            _currentEnemy.defaultColor = _currentEnemy.spriteRenderer.color;

            SetHealth(_currentEnemy.health);
        }
    }

    public void SubscribeToOnDeath(EnemyOneParamDelegate del)
    {
        if (del != null)
            OnDeath += del;
    }

    public void UnsubscribeFromOnDeath(EnemyOneParamDelegate del)
    {
        if (del != null)
            OnDeath -= del;
    }

    void SetHealth(int newHealth)
    {
        if (_currentEnemy.showHealth)
            _currentEnemy.healthText.text = newHealth.ToString();

        _currentEnemy.health = newHealth;
    }

    public void TakeDamage(int count)
    {
        SetHealth(_currentEnemy.health - count);
        if (_currentEnemy.health <= 0)
            Die();
    }

    void Die()
    {
        if (_currentEnemy.split != null)
        {
            for (int i = 0; i < _currentEnemy.split.spawnCount; ++i)
            {
                float projection = 0.1f;
                Vector2 randVec = new Vector2(Random.Range(-10f, 10f), Random.Range(-10f, 10f)).normalized;
                Enemy enemy = (Enemy)PoolManager.Instance.SpawnObjectFromPool("Enemy", _currentEnemy.myRigidbody2D.position + randVec * projection);

                enemy.SetToLevel(_currentEnemy.split.spawnLevel);
            }
        }

        if (OnDeath != null)
        {
            OnDeath.Invoke(this);
        }

        RaiseDeactivateEvent();
    }

    #region Frozen state
    private bool _isFrozen;

    [Header("Frozen State")]
    [SerializeField] private Color _frozenColor;

    public void EnterFreezeState()
    {
        if (_isFrozen == false)
        {
            _isFrozen = true;
            _currentEnemy.spriteRenderer.color = _frozenColor;

            // no collision response
            _currentEnemy.myRigidbody2D.isKinematic = true;
        }
    }

    public void ExitFreezeState()
    {
        if (_isFrozen && _isElectric == false)
        {
            // collision response
            _currentEnemy.myRigidbody2D.isKinematic = false;

            _currentEnemy.spriteRenderer.color = _currentEnemy.defaultColor;
        }

        _isFrozen = false;
    }
    #endregion

    #region Electric state
    private bool _isElectric;

    [Header("Electric Sate")]
    [SerializeField] private Color _electricColor;

    public void EnterElectricState()
    {
        if (_isElectric == false)
        {
            _isElectric = true;
            _currentEnemy.spriteRenderer.color = _electricColor;

            // no collision response
            _currentEnemy.myRigidbody2D.isKinematic = true;
        }
    }

    public void ExitElectricState()
    {
        if (_isElectric && _isFrozen == false)
        {
            // collision response
            _currentEnemy.myRigidbody2D.isKinematic = false;

            _currentEnemy.spriteRenderer.color = _currentEnemy.defaultColor;
        }

        _isElectric = false;
    }
    #endregion

    #region Slow state

    [Header("Slow Sate")]
    [SerializeField] private float _slowSpeed;

    private bool _isSlowed;

    public void EnterSlowState()
    {
        _isSlowed = true;
    }

    public void ExitSlowState()
    {
        _isSlowed = false;
    }
    #endregion
}


public struct EnemyStruct
{
    public int health;
    public float speed;
    public Text healthText;
    public Rigidbody2D myRigidbody2D;
    public SpriteRenderer spriteRenderer;
    public bool showHealth;
    public Color defaultColor;
    public int level;

    public EnemySplitValues split;
}