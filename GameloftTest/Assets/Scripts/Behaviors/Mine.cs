﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : PoolObject
{
    [SerializeField] private float _explosionRadius;
    [SerializeField] private float _explosionDelay;

    [SerializeField] private float _timeToReahTarget;

    public void StartDelayExplosion(Vector2 targetPosition, int dammage)
    {
        StartCoroutine(ExplosionDelayCoroutine(_explosionDelay, dammage));
        StartCoroutine(ReachTargetCoroutine(_explosionRadius, targetPosition));
    }

    void Explose(int dammage)
    {
        LayerMask layer = 1 << LayerMask.NameToLayer("Enemy");
        Collider2D[] res = Physics2D.OverlapCircleAll(transform.position, _explosionRadius, layer);
        foreach (Collider2D collider in res)
        {
            Enemy enemy = collider.transform.GetComponentInParent<Enemy>();
            enemy.TakeDamage(dammage);
        }

        PoolManager.Instance.SpawnObjectFromPool("Explosion", transform.position);
    }

    IEnumerator ReachTargetCoroutine(float duration, Vector2 target)
    {
        float time = 0.0f;
        Vector2 startPosition = transform.position;
        while (time < duration)
        {
            time += Time.deltaTime;
            Vector2 res = Vector2.Lerp(startPosition, target, time / duration);
            transform.position = res;
            yield return null;
        }
    }

    IEnumerator ExplosionDelayCoroutine(float duration, int dammage)
    {
        yield return new WaitForSeconds(duration);

        Explose(dammage);
        RaiseDeactivateEvent();
    }

    public override void EnablePoolObject()
    {
        base.EnablePoolObject();
    }

    public override void ClearObjectReferences()
    {
    }

    public override void UpdateObject()
    {
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _explosionRadius);
    }
}
