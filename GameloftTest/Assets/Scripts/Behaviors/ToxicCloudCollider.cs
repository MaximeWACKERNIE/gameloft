﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicCloudCollider : MonoBehaviour
{
    private ParticleSystem[] _particleSystems;
    private Collider2D _collider;

    private int _damage;
    private float _damageInterval;

    private float _currentTime;

    private void Awake()
    {
        _particleSystems = GetComponentsInChildren<ParticleSystem>();
        _collider = GetComponent<Collider2D>();
    }

    private void OnEnable()
    {
        foreach (ParticleSystem ps in _particleSystems)
        {
            ps.Clear();
            ps.Play();
        }
    }

    private void OnDisable()
    {
        _currentTime = 0.0f;
        _damage = 0;
    }

    private void Update()
    {
        _currentTime += Time.deltaTime;
        if (_currentTime > _damageInterval)
        {
            ApplyDamage();
            _currentTime = 0.0f;
        }
    }

    public void Init(int damage, float interval)
    {
        _damage = damage;
        _damageInterval = interval;
    }

    void ApplyDamage()
    {
        int maxColliders = PoolManager.Instance.GetPool("Enemy").ActiveObjectCount;
        Collider2D[] result = new Collider2D[maxColliders];

        ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.layerMask = 1 << LayerMask.NameToLayer("Enemy");
        contactFilter.useLayerMask = true;
        int count = _collider.OverlapCollider(contactFilter, result);
        for (int i = 0; i < count; ++i)
        {
            Enemy enemy = result[i].transform.parent.GetComponent<Enemy>();
            enemy.TakeDamage(_damage);
        }
    }
}
