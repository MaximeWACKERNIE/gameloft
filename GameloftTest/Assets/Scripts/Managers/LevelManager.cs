﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance
    {
        get { return _instance; }
    }

    [Header("Spawn")]
    [SerializeField] private bool activateAutoSpawn = true;
    [SerializeField] private Transform _spawnParent;
    [SerializeField] private float _minSpawnSlotCooldown;
    [SerializeField] private float _maxSpawnSlotCooldown;

    SpawnSlotStruct[] _spawnSlots;

    delegate void LevelManagerThreeParamDelegate(int id, int level, bool restart);

    private void Awake()
    {
        _instance = this; 

        SpawnSlotStruct slot;
        _spawnSlots = new SpawnSlotStruct[_spawnParent.childCount];
        for (int i = 0; i < _spawnParent.childCount; ++i)
        {
            slot = new SpawnSlotStruct();
            slot.position = _spawnParent.GetChild(i).position;

            _spawnSlots[i] = slot;
        }
    }

    private void Start()
    {
        if (activateAutoSpawn)
        {
            for (int i = 0; i < _spawnParent.childCount; ++i)
            {
                StartSpawn(i, true);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad1))
            SpawnToSlot(0, 1, false);
        if (Input.GetKeyDown(KeyCode.Keypad2))
            SpawnToSlot(0, 2, false);
        if (Input.GetKeyDown(KeyCode.Keypad3))
            SpawnToSlot(0, 3, false);
        if (Input.GetKeyDown(KeyCode.Keypad4))
            SpawnToSlot(0, 4, false);
        if (Input.GetKeyDown(KeyCode.Keypad5))
            SpawnToSlot(0, 5, false);

    }

    void StartSpawn(int id, bool restart)
    {
        float duration = Random.Range(_minSpawnSlotCooldown, _maxSpawnSlotCooldown);
        int level = Random.Range(1, 5);
        StartCoroutine(SpawCooldownCoroutine(duration, id, level, restart, SpawnToSlot));
    }

    IEnumerator SpawCooldownCoroutine(float duration, int id, int level, bool restart, LevelManagerThreeParamDelegate callBack)
    {
        float time = 0.0f;
        while (time < duration)
        {
            time += Time.deltaTime;
            yield return null;
        }

        if (callBack != null)
            callBack.Invoke(id, level, restart);
    }

    void SpawnToSlot(int id, int level, bool restart)
    {
        _spawnSlots[id].inCoolDown = false;

        if (id < _spawnSlots.Length)
        {
            Enemy enemy = (Enemy)PoolManager.Instance.SpawnObjectFromPool("Enemy", _spawnSlots[id].position);
            if (enemy != null)
            {
                enemy.SetToLevel(level);
            }
        }

        if (restart)
            StartSpawn(id, restart);
    }
}

public struct SpawnSlotStruct
{
    public Vector3 position;
    public bool inCoolDown;
}
