﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    private static PoolManager _instance;
    public static PoolManager Instance
    {
        get { return _instance; }
    }

    [SerializeField] List<PoolStruct> _pools;

    private Dictionary<string, GenericPool<PoolObject>> _dictionary;

    private void Awake()
    {
        _instance = this;
        _dictionary = new Dictionary<string, GenericPool<PoolObject>>(_pools.Count);

        foreach (PoolStruct pool in _pools)
        {
            GenericPool<PoolObject> genericPool = new GenericPool<PoolObject>(pool.count, pool.prefab, GenericPool<PoolObject>.OBJECT_CREATION_OPTION.DOES_NOT_CREATE_OBJECT, pool.parent);
            _dictionary.Add(pool.tag, genericPool);
        }
    }

    private void Update()
    {
        foreach (KeyValuePair<string, GenericPool<PoolObject>> entry in _dictionary)
        {
            entry.Value.Update();
        }
    }

    public PoolObject SpawnObjectFromPool(string tag, Vector3 position)
    {
        GenericPool<PoolObject> generic = _dictionary[tag];
        if (generic != null)
        {
            return generic.SpawnObjectFromPool(position);
        }

        Debug.Log("tag: " + tag + " not found");
        return null;
    }

    public GenericPool<PoolObject> GetPool(string tag)
    {
        return _dictionary[tag];
    }
}

[System.Serializable]
public struct PoolStruct
{
    public string tag;
    public int count;
    public Transform parent;
    public PoolObject prefab;
}
