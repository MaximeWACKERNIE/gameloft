﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance
    {
        get { return _instance; }
    }

    [Header("DeadZone")]
    [SerializeField] float _yDeadZoneBullet;
    [SerializeField] float _yDeadZoneEnemy;
    [SerializeField] float _yDangerZoneEnemy;
    [SerializeField] BlinkingImage _dangerZoneImage;

    [Header("Colors")]
    [SerializeField] private List<Color> _bulletColors;
    public Color GetRandomBulletColor()
    {
        if (_bulletColors.Count == 1)
            return Color.black;

        if (_bulletColors.Count == 1)
            return _bulletColors[0];

        return _bulletColors[Random.Range(0, _bulletColors.Count - 1)];
    }

    public float YDeadZoneBullet { get { return _yDeadZoneBullet; } }

    bool _isInAlerStatus;

    private void Awake()
    {
        _instance = this;

        Application.targetFrameRate = 60;
        Physics2D.gravity = Vector2.zero;
    }

    public Enemy GetClosesEnemy(Vector3 position, List<Enemy> toExclude = null)
    {
        float minDistance = float.MaxValue;
        Enemy res = null;
        List<PoolObject> enemies = PoolManager.Instance.GetPool("Enemy").GetAllActiveObjects();
        if (enemies == null)
            return null;

        foreach (Enemy enemy in enemies)
        {
            float distance = Vector3.Distance(position, enemy.CurrentPosition);
            if (distance < minDistance)
            {
                if (toExclude != null)
                {
                    if (toExclude.Contains(enemy) && res != null)
                        continue;

                    res = enemy;
                    continue;
                }

                minDistance = distance;
                res = enemy;
            }
        }

        return res;
    }

    public bool IsInDangerZone(Vector3 position)
    {
        if (position.y < _yDangerZoneEnemy)
        {
            EnterAlerStatus();
            return true;
        }

        return false;
    }

    public bool IsInDeadZone(Vector3 position)
    {
        if (position.y < _yDeadZoneEnemy)
        {
            GameOver();
            return true;
        }

        return false;
    }

    void EnterAlerStatus()
    {
        if (_isInAlerStatus == false)
        {
            _isInAlerStatus = true;
            _dangerZoneImage.StartBlink();
        }
    }

    void ExitAlerStatus()
    {
        if (_isInAlerStatus)
        {
            _isInAlerStatus = false;
            _dangerZoneImage.StopBlink();
        }
    }

    public void OnEnemyDeath(Enemy target)
    {
        if (_isInAlerStatus)
        {
            List<PoolObject> enemies = PoolManager.Instance.GetPool("Enemy").GetAllActiveObjects();
            if (enemies != null)
            {
                foreach (Enemy enemy in enemies)
                {
                    if (IsInDangerZone(enemy.CurrentPosition) && enemy != target)
                        return;
                }
            }

            ExitAlerStatus();
        }
    }

    private void GameOver()
    {
        SceneManager.LoadScene(0);
    }
}
