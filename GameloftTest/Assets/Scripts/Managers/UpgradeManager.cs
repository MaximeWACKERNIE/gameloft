﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour
{
    private static UpgradeManager _instance;
    public static UpgradeManager Instance
    {
        get { return _instance; }
    }

    [SerializeField] Transform _upgradesParent;
    [SerializeField] UpgradeMultiplierData _upgradeMultiplierData;

    private List<ShipUpgrade> _upgrades;

    int _money;

    private void Awake()
    {
        _instance = this;
        _money = 1000000;
    }

    void Start()
    {
        _upgrades = new List<ShipUpgrade>(4);
        for (int i = 0; i < 4; ++i)
            _upgrades.Add(new ShipUpgrade());

        foreach (CharacterShip ship in Character.Instance.Ships)
            ship.SubscribeToLevelUpdated(SetLevel);

        UpdateAllPrices();
    }

    void Update()
    {
    }

    void UpdateAllPrices()
    {
        Transform upgradeTransform;

        for (int i = 0; i < 4; ++i)
        {
            upgradeTransform = _upgradesParent.GetChild(i);
            SetPrices(ref _upgrades[i].damage, upgradeTransform, "Damage");
            SetPrices(ref _upgrades[i].speed, upgradeTransform, "Speed");
        }
    }

    public void UpgradeShipDmg(int id)
    {
        if (id < _upgrades.Count)
        {
            Upgrade(ref _upgrades[id].damage, id, "Damage");
            SetUpgradesBuff(id, _upgrades[id]);
        }
    }

    public void UpgradeShipSpeed(int id)
    {
        if (id < _upgrades.Count)
        {
            Upgrade(ref _upgrades[id].speed, id, "Speed");
            SetUpgradesBuff(id, _upgrades[id]);
        }
    }

    void Upgrade(ref ShipUpgradeData toUpgrade, int id, string type)
    {
        if (_money >= toUpgrade.cost && toUpgrade.count < _upgradeMultiplierData.maxUpgrades)
        {
            Transform upgradeTransform = _upgradesParent.GetChild(id);

            _money -= toUpgrade.cost;
            ++toUpgrade.count;
            SetPrices(ref toUpgrade, upgradeTransform, type);
        }
    }

    void SetPrices(ref ShipUpgradeData toSet, Transform tr, string type)
    {
        toSet.cost = (int)(_upgradeMultiplierData.baseCost * Mathf.Pow(_upgradeMultiplierData.multiplier, toSet.count));

        Transform ui = tr.Find(type);
        ui.GetComponentInChildren<Text>().text = toSet.cost.ToString();

        /*SaveManager.SaveContainer._fuelUpgrade = _fuel._data;
        SaveManager.SaveContainer._scorePerSecUpgrade = _scorePerSec._data;
        SaveManager.Save();*/
    }

    void SetUpgradesBuff(int id, ShipUpgrade toSet)
    {
        Character.Instance.UpdateShip(id, toSet);
    }

    void SetLevel(int id, int level)
    {
        Transform upgradeTransform = _upgradesParent.GetChild(id);
        Transform ui = upgradeTransform.Find("Level");
        ui.GetComponentInChildren<Text>().text = level.ToString();

    }
}
